﻿namespace back_end.Infrastructure
{
    using back_end.Application;
    using back_end.Infrastructure.AllTheClouds;

    public class AppConfiguration
    {
        public AllTheCloudsConfiguration AllTheClouds { get; set; }
        public MarkUpConfiguration MarkUp { get; set; }
    }
}
