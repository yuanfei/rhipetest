﻿namespace back_end.Infrastructure.AllTheClouds
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using AutoMapper;
    using back_end.Application.Interface;
    using back_end.Domain;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;

    public class AllTheCloudsProductProvider : IProductsProvider
    {
        private IMapper mMapper;
        private AllTheCloudsConfiguration mConfiguration;
        private IHttpClientFactory mHttpClientFactory;
        private ILogger<AllTheCloudsProductProvider> mLogger;

        public AllTheCloudsProductProvider(IMapper mapper,
            AllTheCloudsConfiguration config,
            IHttpClientFactory factory,
            ILogger<AllTheCloudsProductProvider> logger)
        {
            mMapper = mapper;
            mConfiguration = config;
            mHttpClientFactory = factory;
            mLogger = logger;
        }
        
        public async Task<List<Product>> GetProducts()
        {
            const string uri = "api/Products";
            var httpClient = mHttpClientFactory.CreateClient("AllTheCloudsClient");
            httpClient.BaseAddress = new Uri(mConfiguration.BaseAddress);
            httpClient.DefaultRequestHeaders.Add(mConfiguration.ApiKeyHeader, mConfiguration.ApiKeyValue);

            var result = await httpClient.GetAsync(uri);
            var resultString = await result.Content.ReadAsStringAsync();

            if (result.IsSuccessStatusCode)
            {
                var allTheCloudsProducts = JsonConvert.DeserializeObject<List<AllTheCloudsProduct>>(resultString);
                var allProducts = mMapper.Map<List<AllTheCloudsProduct>, List<Product>>(allTheCloudsProducts);
                mLogger.LogInformation($"Get {allProducts.Count} AllTheCloudsProduct");
                return allProducts;
            }

            var msg = $"Failed to get product from {mConfiguration.BaseAddress}";
            mLogger.LogError(msg);
            throw new BadHttpRequestException(msg);
        }
    }
}
