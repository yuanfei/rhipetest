﻿namespace back_end.Infrastructure.AllTheClouds
{
    using AutoMapper;
    using back_end.Domain;

    public class AllTheCloudsProductMapperProfile : Profile
    {
        public AllTheCloudsProductMapperProfile()
        {
            CreateMap<AllTheCloudsProduct, Product>();
        }
    }
}
