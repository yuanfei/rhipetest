﻿namespace back_end.Infrastructure.AllTheClouds
{
    public class AllTheCloudsConfiguration
    {
        public string BaseAddress { get; set; }
        public string ApiKeyHeader { get; set; }
        public string ApiKeyValue { get; set; }
    }
}
