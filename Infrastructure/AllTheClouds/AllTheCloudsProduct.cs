﻿namespace back_end.Infrastructure.AllTheClouds
{
    public class AllTheCloudsProduct
    {
        public string ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal UnitPrice { get; set; }
        public ulong? MaximumQuantity { get; set; }
    }
}
