﻿namespace back_end.Infrastructure
{
    using Autofac;
    using back_end.Infrastructure.AllTheClouds;

    public class InfrastructureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.Register(c => c.Resolve<AppConfiguration>().AllTheClouds)
                .SingleInstance();
            builder.Register(c => c.Resolve<AppConfiguration>().MarkUp)
                .SingleInstance();
            builder.RegisterType<AllTheCloudsProductProvider>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
