﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace back_end.Controllers
{
    using back_end.Application.Queries;
    using MediatR;

    [ApiController]
    [Route("Product")]
    public class ProductsController : ControllerBase
    {
        private IMediator mMediator;

        public ProductsController(IMediator mediator)
        {
            mMediator = mediator;
        }
        
        [HttpGet]
        [Route("All")]
        public async Task<ActionResult<IEnumerable<ProductDto>>> GetAllProducts()
        {
            var result = await mMediator.Send(new GetAllProductsQuery());
            return Ok(result);

        }
    }
}
