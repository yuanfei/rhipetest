﻿namespace back_end.Application.Queries
{
    public class ProductDto
    {
        public string ProductId { get; set; }
        public string Description { get; set; }
        public decimal UnitPrice { get; set; }
    }
}
