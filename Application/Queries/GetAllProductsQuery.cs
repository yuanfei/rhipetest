﻿using System.Collections.Generic;

namespace back_end.Application.Queries
{
    using MediatR;

    public class GetAllProductsQuery : IRequest<IEnumerable<ProductDto>>
    {
    }
}
