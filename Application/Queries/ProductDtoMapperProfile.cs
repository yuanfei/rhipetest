﻿namespace back_end.Application.Queries
{
    using AutoMapper;
    using back_end.Domain;

    public class ProductDtoMapperProfile : Profile
    {
        public ProductDtoMapperProfile()
        {
            CreateMap<Product, ProductDto>();
        }

    }
}
