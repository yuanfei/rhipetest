﻿namespace back_end.Application.Queries
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Threading;
    using AutoMapper;
    using back_end.Application.Interface;
    using back_end.Domain;
    using MediatR;
    using Microsoft.Extensions.Logging;

    public class GetAllProductsQueryHandler : IRequestHandler<GetAllProductsQuery, IEnumerable<ProductDto>>
    {
        private readonly IMapper mMapper;
        private readonly IProductsProvider mProductsProvider;
        private readonly MarkUpConfiguration mMarkUpConfiguration;
        private ILogger<GetAllProductsQueryHandler> mLogger;

        public GetAllProductsQueryHandler(IMapper mapper,
            IProductsProvider productsProvider,
            MarkUpConfiguration configuration,
            ILogger<GetAllProductsQueryHandler> logger)
        {
            mMapper = mapper;
            mProductsProvider = productsProvider;
            mMarkUpConfiguration = configuration;
            mLogger = logger;
        }
        public async Task<IEnumerable<ProductDto>> Handle(GetAllProductsQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var allProducts =  await mProductsProvider.GetProducts();
                allProducts.ForEach(p => p.MarkUp(mMarkUpConfiguration.Rate));
                mLogger.LogInformation($"Marked Up price by {mMarkUpConfiguration.Rate} ");
                var result = mMapper.Map<List<ProductDto>>(allProducts);
                return result.AsEnumerable();
            }
            catch (Exception)
            {
                mLogger.LogError("Failed to get product from AllTheClouds");
                return new List<ProductDto>();
            }
        }
    }
}
