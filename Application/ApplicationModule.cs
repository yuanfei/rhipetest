﻿namespace back_end.Application
{
    using System.Reflection;
    using Autofac;
    using back_end.Application.Queries;
    using MediatR;

    public class ApplicationModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<GetAllProductsQuery>()
                .AsSelf()
                .InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly)
                .AsImplementedInterfaces();
        }
    }
}
