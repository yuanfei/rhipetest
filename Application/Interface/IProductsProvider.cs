﻿namespace back_end.Application.Interface
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using back_end.Domain;

    public interface IProductsProvider
    {
        Task<List<Product>> GetProducts();
    }
}