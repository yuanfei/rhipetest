namespace back_end.tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using back_end.Application;
    using back_end.Application.Interface;
    using back_end.Application.Queries;
    using back_end.Domain;
    using back_end.Infrastructure.AllTheClouds;
    using FluentAssertions;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Logging;
    using Moq;
    using Xunit;

    public class GetAllProductsQueryHandlerTests : MapperFixture
    {
        [Fact]
        public async Task WhenProviderSuccess_ShouldReturnAllProductDto()
        {
            var productProvider = new Mock<IProductsProvider>();
            productProvider.Setup(m => m.GetProducts())
                .Returns(Task.FromResult(new List<Product>()
                {
                    new Product()
                    {
                        Description = "test1",
                        Markup = 0m,
                        MaximumQuantity = 100,
                        Name = "test1",
                        ProductId = "1",
                        UnitPrice = 1.0m

                    },
                    new Product()
                    {
                        Description = "test2",
                        Markup = 0m,
                        MaximumQuantity = 100,
                        Name = "test2",
                        ProductId = "2",
                        UnitPrice = 2.0m

                    }
                }));

            var markup = new MarkUpConfiguration()
            {
                Rate = 1.2m,
            };

            var handler = new GetAllProductsQueryHandler(mMapper, productProvider.Object, markup, new Mock<ILogger<GetAllProductsQueryHandler>>().Object);

            var result = await handler.Handle(new GetAllProductsQuery(), CancellationToken.None);

            result.Should().BeEquivalentTo(new List<ProductDto>()
            {
                new ProductDto()
                {
                    Description = "test1",
                    ProductId = "1",
                    UnitPrice = 1.2m

                },
                new ProductDto()
                {
                    Description = "test2",
                    ProductId = "2",
                    UnitPrice = 2.4m
                }
            });
        }

        [Fact]
        public async Task WhenProviderFail_ShouldReturnEmptyList()
        {
            var productProvider = new Mock<IProductsProvider>();
            productProvider.Setup(m => m.GetProducts())
                .Throws(new BadHttpRequestException("failed"));

            var markup = new MarkUpConfiguration()
            {
                Rate = 1.2m,
            };

            var handler = new GetAllProductsQueryHandler(mMapper, productProvider.Object, markup, new Mock<ILogger<GetAllProductsQueryHandler>>().Object);

            var result = await handler.Handle(new GetAllProductsQuery(), CancellationToken.None);

            result.Count().Should().Be(0);
        }
    }
}
