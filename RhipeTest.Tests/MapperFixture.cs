﻿namespace back_end.tests
{
    using AutoMapper;
    using back_end.Application.Queries;
    using back_end.Infrastructure.AllTheClouds;

    public class MapperFixture
    {
        protected IMapper mMapper;

        public MapperFixture()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AllTheCloudsProductMapperProfile());
                cfg.AddProfile(new ProductDtoMapperProfile());
            });
            mMapper = config.CreateMapper();
        }
    }
}