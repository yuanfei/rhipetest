﻿namespace back_end.tests.Infrastructure.AllTheClouds
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using back_end.Infrastructure.AllTheClouds;
    using FluentAssertions;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Logging;
    using Moq;
    using Moq.Protected;
    using Xunit;

    public class AllTheCloudsProviderTests : MapperFixture
    {
        public AllTheCloudsProviderTests()
        {
        }

        [Fact]
        public async Task WhenConstruct_ShouldConfigHttpClientWithRightHeader()
        {
            var httpMessageHandlerMock = new Mock<HttpMessageHandler>();
            httpMessageHandlerMock.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .Returns(()=> Task.FromResult(new HttpResponseMessage(HttpStatusCode.OK)));

            var httpClient = new HttpClient(httpMessageHandlerMock.Object);

            var clientFactoryMock = new Mock<IHttpClientFactory>();
            clientFactoryMock.Setup(m => m.CreateClient(It.IsAny<string>()))
                .Returns(httpClient);
            var config = new AllTheCloudsConfiguration()
            {
                BaseAddress = "http://dummy",
                ApiKeyHeader = "header",
                ApiKeyValue = "dummy"
            };

            var allTheCloudsProvider = new AllTheCloudsProductProvider(mMapper, 
                config, 
                clientFactoryMock.Object, new Mock<ILogger<AllTheCloudsProductProvider>>().Object);

            await allTheCloudsProvider.GetProducts();

            httpClient.DefaultRequestHeaders.Should().Contain(x => x.Key == "header" && x.Value.Contains("dummy"));
        }

        [Fact]
        public async Task WhenFailedToGetProducts_ShouldThrow()
        {
            var httpMessageHandlerMock = new Mock<HttpMessageHandler>();
            httpMessageHandlerMock.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .Returns(()=> Task.FromResult(new HttpResponseMessage(HttpStatusCode.BadRequest)));

            var httpClient = new HttpClient(httpMessageHandlerMock.Object);

            var clientFactoryMock = new Mock<IHttpClientFactory>();
            clientFactoryMock.Setup(m => m.CreateClient(It.IsAny<string>()))
                .Returns(httpClient);
            var config = new AllTheCloudsConfiguration()
            {
                BaseAddress = "http://dummy",
                ApiKeyHeader = "header",
                ApiKeyValue = "dummy"
            };

            var allTheCloudsProvider = new AllTheCloudsProductProvider(mMapper, 
                config, 
                clientFactoryMock.Object, new Mock<ILogger<AllTheCloudsProductProvider>>().Object);

            Func<Task> func = async () => await allTheCloudsProvider.GetProducts();

            await func.Should().ThrowAsync<BadHttpRequestException>();
        }

    }
}