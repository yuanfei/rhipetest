import { configureStore } from '@reduxjs/toolkit';
import { productReducer } from '../features/ProductPage/productPageReducer'

export const store = configureStore({
  reducer: productReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware(),
});
