import configureStore from 'redux-mock-store';
import React from 'react';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import {ProductPage} from '../ProductPage';
import { LOAD_PRODUCTS } from '../ProductPageAction';
import { mount } from 'enzyme';


const mockStore = configureStore([]);

describe('ProductPage', () => {
    beforeEach(() => {
        fetch.resetMocks();
    });

    it('When get 0 products should show warning line', ()=>{
        fetch.mockResponseOnce(JSON.stringify([]));

        const store = mockStore({products:[]});

        const sut = mount(
            <Provider store={store}>
                <ProductPage></ProductPage>
            </Provider>
        )

        expect(fetch).toHaveBeenCalledWith('https://localhost:5001/product/all');
   
        expect(sut.find('p').first().text()).toEqual("There is no product available now");
    })
})