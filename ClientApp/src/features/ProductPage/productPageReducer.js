import { LOAD_PRODUCTS } from "./ProductPageAction";
import { createReducer } from "@reduxjs/toolkit";

const defaultState = {
    products:[]
}

export const productReducer = createReducer(defaultState, (builder) => {
    builder
      .addCase(LOAD_PRODUCTS, (state, action) => ({
        ...state,
        products: action.products
      }))      
  })