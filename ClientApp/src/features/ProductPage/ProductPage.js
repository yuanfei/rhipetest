import React, {useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getProductList } from './productPageSelector';
import { loadProduct } from './ProductPageApi';
import MaterialTable from 'material-table'

export function ProductPage() {
  const productList = useSelector(getProductList) || [];

  const dispatch = useDispatch();
  
   // eslint-disable-next-line
  useEffect(()=> { loadProduct(dispatch); },[]);
  
  const productTableData = productList.map(  product => ({
    description: product.description,
    price: product.unitPrice,
  }));

  const showWarning = productTableData.length === 0;

  return (
    <div>
    {showWarning && <div><p>There is no product available now</p></div>}
    <MaterialTable
          options={{search: false}}
          columns={[
            { title: 'Description', field: 'description' },
            { title: 'Price', field: 'price' },
          ]}
          data={productTableData}
          title="All Products"
        />
    </div>
  );
}
