import { LOAD_PRODUCTS } from "./ProductPageAction";
import Config from "../../config"

export const loadProduct = async (dispatch) => {
    await fetch(Config.SERVER_URL +"/product/all")
    .then(async (res)=>{
      if (res.status === 200) {
        const resData = await res.json();
        dispatch({type:LOAD_PRODUCTS, products:resData})
      } else {
        dispatch(dispatch({type:LOAD_PRODUCTS, products:[]}))
      }    
    })
    .catch(()=>{
      dispatch(dispatch({type:LOAD_PRODUCTS, products:[]}))
    });
    
  };