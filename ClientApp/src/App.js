import React from 'react';
import './App.css';
import { ProductPage } from './features/ProductPage/ProductPage';

function App() {
  return (
    <div className="App">
      <ProductPage/>
    </div>
  );
}

export default App;
