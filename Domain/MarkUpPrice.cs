﻿namespace back_end.Domain
{
    public static class MarkUpPrice
    {
        public static void MarkUp(this Product product, decimal rate)
        {
            product.UnitPrice = product.UnitPrice * rate;
            product.Markup = rate;
        }
    }
}
