﻿namespace back_end.Domain
{
    public class Product
    {
        public string ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal UnitPrice { get; set; }
        public ulong? MaximumQuantity { get; set; }
        public decimal Markup { get; set; }
    }
}